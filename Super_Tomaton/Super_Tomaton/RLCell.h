//
//  RLCell.h
//  Super_Tomaton
//
//  Created by Carlos Flores on 07/03/14.
//  Copyright (c) 2014 Rafael_Lopez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RLCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *subTitle;
@property (weak, nonatomic) IBOutlet UIImageView  *imageMovieBg;

@end

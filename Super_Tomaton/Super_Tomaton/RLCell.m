//
//  RLCell.m
//  Super_Tomaton
//
//  Created by Carlos Flores on 07/03/14.
//  Copyright (c) 2014 Rafael_Lopez. All rights reserved.
//

#import "RLCell.h"

@implementation RLCell
@synthesize subTitle = _subTitle;
@synthesize title = _title;
@synthesize imageMovieBg = _imageMovieBg;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

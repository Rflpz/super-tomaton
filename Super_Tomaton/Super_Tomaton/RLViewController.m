//
//  RLViewController.m
//  Super_Tomaton
//
//  Created by ios on 04/03/14.
//  Copyright (c) 2014 Rafael_Lopez. All rights reserved.
//

#pragma mark - Import files and frameworks
#import "RLViewController.h"
#import "RLSecondViewController.h"
#import "UIImageView+AFNetworking.h"
#import "RLCell.h"
#import "UIScrollView+SVInfiniteScrolling.h"

#pragma mark - Create url for download the data
static NSString *URList = @"http://api.rottentomatoes.com/api/public/v1.0/lists/movies/in_theaters.json?apikey=hpabgrq5jhkvpy32kfc9t3wj";
static NSString *firstUrl = @"http://api.rottentomatoes.com/api/public/v1.0/lists/movies/in_theaters.json?apikey=hpabgrq5jhkvpy32kfc9t3wj";
static NSString *APIKey = @"?apikey=hpabgrq5jhkvpy32kfc9t3wj";
NSInteger totalPagesGet = 1;
#pragma mark - Generate the propertys for the view and code
@interface RLViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic)  UIRefreshControl *refreshControl;
@property (strong, nonatomic)  NSIndexPath *lastSelectedIndexPath;
@property (strong, nonatomic) NSArray *arrayData;
@property (weak, nonatomic) NSString *query;
@end

@implementation RLViewController

#pragma mark - Set all the interface builder
- (void)viewDidLoad
{
    [super viewDidLoad];
    totalPagesGet=1;
    //Infinite scrolling
    __weak RLViewController *weakSelf = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
    //Customize navController
    [[UINavigationBar appearance] setTitleTextAttributes: @{NSFontAttributeName: [UIFont fontWithName:@"Avenir-Medium" size:18.0f],}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    
    //Customize view
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"bg.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    self.title = @"Tomaton";
    
    
    //Customize search bar
    self.searchBar.barTintColor = [UIColor colorWithRed:255/256.0 green:255/256.0 blue:255/256.0 alpha:.10];
    self.searchBar.alpha = .80;
    UIGraphicsBeginImageContext(self.searchBar.frame.size);
    [[UIImage imageNamed:@"searchBar.png"] drawInRect:self.searchBar.bounds];
    UIImage *imageSearch = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.searchBar.backgroundImage = imageSearch;
    
    
    //Customize tableView
    UIGraphicsBeginImageContext(self.tableView.frame.size);
    [[UIImage imageNamed:@"sombra.png"] drawInRect:self.tableView.bounds];
    UIImage *imageTable = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:imageTable];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self customizeViewInterface];
}

#pragma  mark - Create to refreshControl and add subview to table view
- (void)customizeViewInterface {
    UIRefreshControl *refreshControl= [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self
                       action:@selector(refreshInfo:)
             forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    self.refreshControl =refreshControl;
    
    
}

#pragma mark - Call to the refresh control
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView deselectRowAtIndexPath:self.lastSelectedIndexPath animated:YES];
    [self.refreshControl beginRefreshing];
    [self refreshInfo:self.refreshControl];
    
}

#pragma mark - Download the JSON and save in NSDictironary
- (void)refreshInfo:(UIRefreshControl *) sender {
    self.refreshControl.tintColor = [UIColor whiteColor];
    self.tableView.separatorColor = [UIColor colorWithRed:0/256.0 green:0/256.0  blue:0/256.0  alpha:0.3];
    NSURLSession *session =[NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:URList]
                                            completionHandler:^(
                                                                NSData *data,
                                                                NSURLResponse *response,
                                                                NSError *error) {
                                                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                NSArray *movies =  [json valueForKeyPath:@"movies"];
                                                NSSortDescriptor *arrayTommatometer = [[NSSortDescriptor alloc] initWithKey:@"ratings.critics_score" ascending:NO];
                                                NSArray *arraycriticScore = [NSArray arrayWithObject:arrayTommatometer];
                                                if (!error) {
                                                    self.arrayData = [movies sortedArrayUsingDescriptors:arraycriticScore];
                                                    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                                                        [self.tableView reloadData];
                                                        [sender endRefreshing];
                                                    }];
                                                }
                                                else {
                                                    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                                                        [sender endRefreshing];
                                                        UIAlertView *help = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                       message:@"Verifica tu conexion a internet"
                                                                                                      delegate:nil
                                                                                             cancelButtonTitle:@"Ok"
                                                                                             otherButtonTitles: nil];
                                                        [help show];
                                                    }];
                                                }
                                            }];
    [dataTask resume];
}

#pragma  mark - Return all the elements in the array
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrayData.count;
}

#pragma mark - Custom  the UITableView
-(UITableViewCell *)tableView:(UITableViewCell *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellId = @"RLCell";
    RLCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellId];
    self.refreshControl.tintColor = [UIColor whiteColor];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"RLCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    NSDictionary *dataInfo = self.arrayData [indexPath.row];
    NSString *cadImagen = [dataInfo valueForKeyPath:@"posters.thumbnail"];
    NSURL *urlImagen = [NSURL URLWithString:cadImagen];
    UIImage *imagen = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlImagen]];
    cell.imageMovieBg.image = imagen;
    
    NSString *criticsRating = [dataInfo valueForKeyPath:@"ratings.critics_rating"];
    NSString *tomatometer;
    NSString *detailsMovie;
    if(criticsRating == NULL){
        criticsRating = @"";
        tomatometer = [[dataInfo valueForKeyPath:@"ratings.critics_score"] stringValue];
        detailsMovie = [NSString stringWithFormat:@"%@%@%@", @" ",@"Rating: ", tomatometer];
    }
    else
    {
        tomatometer = [[dataInfo valueForKeyPath:@"ratings.critics_score"] stringValue];
        detailsMovie = [NSString stringWithFormat:@"%@%@%@%@", @" ", criticsRating,@" - rating: ", tomatometer];
        
    }
    cell.title.text = dataInfo [@"title"];
    cell.subTitle.text = detailsMovie;
    cell.backgroundColor = [UIColor clearColor];
    self.tableView.separatorColor = [UIColor colorWithRed:0/256.0 green:0/256.0  blue:0/256.0  alpha:0.3];
    
    
    return cell;
}

#pragma mark - Did select cell from UITableView
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.lastSelectedIndexPath = indexPath;
    
    NSDictionary *movieSelected = self.arrayData [indexPath.row];
    NSString *movSelect = [movieSelected valueForKeyPath:@"links.self"];
    NSString *movieInfoTotalSelected = [NSString stringWithFormat:@"%@%@", movSelect, APIKey];
    RLSecondViewController *secondController = [[RLSecondViewController alloc]initWithNibName:@"RLSecondViewController" bundle:nil];
    secondController.backURL = movieInfoTotalSelected;
    [self.navigationController pushViewController:secondController animated:YES];
    
}

#pragma mark - Touch the UISearchBar
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    self.query = self.searchBar.text;
    self.query = [self.query stringByReplacingOccurrencesOfString:@" " withString:@"&"];
    NSString *busqueda = @"http://api.rottentomatoes.com/api/public/v1.0/movies.json?apikey=hpabgrq5jhkvpy32kfc9t3wj&q=";
    URList = [NSString stringWithFormat:@"%@%@",busqueda, self.query];
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
    [searchBar resignFirstResponder];
    self.tableView.allowsSelection = YES;
    self.tableView.scrollEnabled = YES;
 
    UIImage* buttonImage = [UIImage imageNamed: @"back_off.png"];
    UIButton *buttonModel = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonModel setImage:buttonImage forState:UIControlStateNormal];
    buttonModel.frame = CGRectMake(0.0, 20.0, 44.0, 44.0);
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:buttonModel];
    [buttonModel addTarget:self action:@selector(backtoMainMenu) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = backButton;
    
    
    [self.refreshControl beginRefreshing];
    [self refreshInfo:self.refreshControl];
    
}

#pragma mark - Action for button cancel at UISearchBar
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    self.tableView.allowsSelection = YES;
    self.tableView.scrollEnabled = YES;
}

#pragma mark - Edit the text at UISearchBar
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
    self.tableView.allowsSelection = NO;
    self.tableView.scrollEnabled = NO;
}

#pragma mark - Back to menu
-(void)backtoMainMenu
{
    NSLog(@"Funcionando");
    URList = firstUrl;
    self.navigationItem.leftBarButtonItem = nil;
    [self.refreshControl beginRefreshing];
    [self refreshInfo:self.refreshControl];
}

#pragma mark - Infinite Scrolling
- (void)insertRowAtBottom {
    totalPagesGet++;
    NSString *newUrl = [NSString stringWithFormat:@"%@%@%ld", URList, @"&page=",(long)totalPagesGet];
    __weak RLViewController *weakSelf = self;
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:[NSURL URLWithString:newUrl]
                                            completionHandler:^(
                                                                NSData *data,
                                                                NSURLResponse *response,
                                                                NSError *error) {
                                                if (!error){
                                                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                    NSSortDescriptor *arrayTommatometer = [NSSortDescriptor sortDescriptorWithKey:@"ratings.critics_score" ascending :NO];
                                                    NSMutableArray *movies= [json valueForKeyPath:@"movies"];
                                                    NSArray *descriptors = [NSArray arrayWithObject:arrayTommatometer];
                                                    NSArray *peliculasOrdenadas=[movies sortedArrayUsingDescriptors:descriptors];
                                                    NSMutableArray *all = [[NSMutableArray alloc]init];
                                                    [all addObjectsFromArray:self.arrayData];
                                                    [all addObjectsFromArray:peliculasOrdenadas];
                                                    self.arrayData = all;
                                                }
                                                else{
                                                    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                                                        UIAlertView *alert = [[UIAlertView alloc]
                                                                              initWithTitle:@"Error"
                                                                              message:@"Verifica tu conexion a internet"
                                                                              delegate:nil
                                                                              cancelButtonTitle:@"Ok"
                                                                              otherButtonTitles: nil];
                                                        [alert show];}];
                                                }
                                            }
                                      ];
    [dataTask resume];
    [self.tableView reloadData];
    [weakSelf.tableView.infiniteScrollingView stopAnimating];
}
@end

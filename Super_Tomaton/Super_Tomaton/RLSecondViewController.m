//
//  RLSecondViewController.m
//  Super_Tomaton
//
//  Created by ios on 04/03/14.
//  Copyright (c) 2014 Rafael_Lopez. All rights reserved.
//
#pragma mark - Import the files and frameworks
#import "RLSecondViewController.h"
#import "UIImageView+AFNetworking.h"
#import <QuartzCore/QuartzCore.h>


#pragma mark - Generate the propertys for the view and code
@interface RLSecondViewController ()
@property (weak, nonatomic) IBOutlet UITextView *synopsis;
@property (weak, nonatomic) IBOutlet UIImageView *background;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *rating;
@end

@implementation RLSecondViewController

#pragma mark - Set all the interface builder
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"secondController.png"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    self.title = @"Detail movie";
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0,0,44,44);
    [button setBackgroundImage:[UIImage imageNamed:@"back_off.png"] forState:UIControlStateNormal];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [button addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [self.navigationItem setLeftBarButtonItem:barButtonItem];
    
    
}
#pragma mark - Assing the data for the view and download de information from the API
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    NSURLSession *sesion = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [sesion dataTaskWithURL:[NSURL URLWithString:self.backURL] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"%@",json);
        if (!error){
            [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                self.name.text = [NSString stringWithFormat:@"%@%@%@", [json valueForKeyPath:@"title"], @" ",[json valueForKeyPath:@"year"]];
                self.synopsis.text = [json valueForKeyPath:@"synopsis"];
                self.rating.text = [json valueForKeyPath:@"ratings.audience_rating"];
                self.rating.text = [self.rating.text lowercaseString];
                NSLog(@"Url url : %@", self.backURL);
                [self.background setImageWithURL:[NSURL URLWithString:[json valueForKeyPath:@"posters.original"]] placeholderImage:nil];
            }];
        }
        else{
            NSLog(@"Hubo un error %@", [error localizedDescription]);
            [[NSOperationQueue mainQueue] addOperationWithBlock:^(void)
             {
                 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Verifica tu conexion a internet" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [alert show];
             }];
        }
        
    }];
    [dataTask resume];
    self.name.textColor = [UIColor whiteColor];
    self.rating.textColor = [UIColor whiteColor];
    self.synopsis.alpha = 0.60;
    self.synopsis.editable = false;
    self.synopsis.selectable = false;
    self.synopsis.backgroundColor = [UIColor clearColor];
    self.synopsis.textColor = [UIColor whiteColor];
    self.synopsis.textAlignment = NSTextAlignmentJustified;
    self.background.alpha = .80;
    self.background.alpha = .65;
}


@end
